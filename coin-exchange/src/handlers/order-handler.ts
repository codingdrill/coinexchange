import CoinCurrency from "../common/core/coin-currency";
import OrderStatus from "../common/core/order-status";
import InsertManager from "../db/insert-manager";
import QueryManager from "../db/query-manager";
import OrderRecord from "../models/order-record";
import OrderRequest from "../models/order-request";
import TradingManager from "../trade/trading-manager";

export function createOrder(order: OrderRequest, cb: (record: OrderRecord) => void) {

  // TBD:
  // Make sure first that the user has enough fund in his/her wallet
  //    to place this order in this particular currency.
  // ...

  const now = new Date();
  InsertManager.createOrder(order.uid,
    order.side,
    order.variety,
    order.target,
    order.exchangeCurrency,
    now,
    OrderStatus.Open,
    (orderId) => {
      console.log(`Order created: orderId = ${orderId}`);
      if (orderId) {
        // true: look for a match.
        getOrder(orderId, true, cb);
      } else {
        if (cb) {
          cb(null);
        } else {
          console.log("WARNING: createOrder() - callback is null.");
        }
      }
    });
}

// Return the specified order to the caller,
// and trigger matching/trading, if this was called from createOrder().
export function getOrder(id: string, justCreated: boolean = false, cb: (record: OrderRecord) => void) {
  QueryManager.getOrder(id, (result: {}) => {
    console.log("result = ", result);
    if (cb) {
      let record: OrderRecord = null;
      if (result) {
        record = OrderRecord.makeOrderRecord(result);
        if (justCreated) {
          // Trigger trading "asynchronously"...
          setTimeout(() => {
            TradingManager.Instance.match(record);
          }, 0);
        }
      }
      cb(record);
    } else {
      console.log("WARNING: getOrder() - callback is null.");
    }
  });
}

export function getOpenOrdersForCurrencies(targetCurrency: CoinCurrency,
                                           exchangeCurrency: CoinCurrency, cb: (records: OrderRecord[]) => void) {
  QueryManager.getOrdersForCurrencies(targetCurrency, exchangeCurrency, OrderStatus.Open, (result: any[]) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const records: OrderRecord[] = [];
        for (const r of result) {
          const record = OrderRecord.makeOrderRecord(r);
          records.push(record);
        }
        cb(records);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getOpenOrdersForCurrencies() - callback is null.");
    }
  });
}

export function getUserOrders(uid: string, cb: (records: OrderRecord[]) => void) {
  QueryManager.getUserOrders(uid, (result: any[]) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const records: OrderRecord[] = [];
        for (const r of result) {
          const record = OrderRecord.makeOrderRecord(r);
          records.push(record);
        }
        cb(records);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getUserOrders() - callback is null.");
    }
  });
}
