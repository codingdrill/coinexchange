import cookieParser from "cookie-parser";
import express from "express";
import logger from "morgan";
import path from "path";

import DatabaseManager from "./db/database-manager";
import adminRouter from "./routes/admin";
import ordersRouter from "./routes/orders";
import ratesRouter from "./routes/rates";
import swaggerRouter from "./routes/swagger";
import transactionsRouter from "./routes/transactions";
import usersRouter from "./routes/users";

const app = express();

// Create a new SQLite DB instance in memory.
const dbCreated = DatabaseManager.createTables();
if (!dbCreated) {
  console.log(">>> SQlite in-memory DB initialization failed. It's best to restart the server.");
}

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", swaggerRouter);
app.use("/users", usersRouter);
app.use("/rates", ratesRouter);
app.use("/orders", ordersRouter);
app.use("/transactions", transactionsRouter);
app.use("/admin", adminRouter);

// export default app;
module.exports = app;
