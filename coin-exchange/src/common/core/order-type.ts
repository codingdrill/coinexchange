enum OrderType {
  Market = 1,
  Limit,
}

export default OrderType;
