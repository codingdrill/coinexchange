import express, { NextFunction, Request, Response } from "express";

import * as TransactionHandler from "../handlers/transaction-handler";
import TransactionRecord from "../models/transaction-record";
import ENV from "../util/env";

const router = express.Router();

// Transactions
router.route("/:transactionId").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const transactionId = req.params.transactionId;
  console.log("transactionId = ", req.params.transactionId);

  TransactionHandler.getTransaction(transactionId, (record: TransactionRecord) => {
    console.log("record = ", record);
    if (record) {
      res.json(record);
    } else {
      res.status(404).json({ error: `Transaction not found for transactionId = ${transactionId}` });
    }
  });
});

export default router;
