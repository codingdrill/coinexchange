import express, { NextFunction, Request, Response } from "express";
import Joi from "joi";

import CurrencyRegsitry from "../common/currency-registry";
import * as OrderHandler from "../handlers/order-handler";
import * as TransactionHandler from "../handlers/transaction-handler";
import * as UserHandler from "../handlers/user-handler";
import DecimalHelper from "../helpers/decimal-helper";
import CurrencyWallet from "../models/currency-wallet";
import OrderRecord from "../models/order-record";
import TransactionRecord from "../models/transaction-record";
import UserWallet from "../models/user-wallet";
import ENV from "../util/env";

const router = express.Router();

// Get user wallet
router.route("/:uid/wallet").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const uid = req.params.uid;
  console.log(`uid = ${uid}`);

  UserHandler.getWallet(uid, (record: UserWallet) => {
    console.log("record = ", record);
    if (record) {
      res.json(record);
    } else {
      res.status(404).json({ error: `Wallet not found for uid = ${uid}` });
    }
  });
});

// Add money to wallet
router.route("/:uid/wallet").post((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const uid = req.params.uid;
  console.log(`uid = ${uid}`);

  const body = req.body;
  if (!body) {
    res.status(400).json({ error: "Request body is missing." });
    return;
  }
  const validated = Joi.validate(body, CurrencyWallet.schema);
  if (validated.error) {
    res.status(400).json({ error: validated.error.details[0].message });
    return;
  }
  const currencyWallet = validated.value as CurrencyWallet;
  if (uid !== currencyWallet.uid) {
    res.status(400).json({
      error: `Invalid request: Path uid, ${uid}, should match the request body uid, ${currencyWallet.uid}`
    });
    return;
  }
  const value = currencyWallet.value;
  const depositAmount = value.amount;
  if (!CurrencyRegsitry.isValid(value.currency)) {  // probably this check is unncessary?
    res.status(400).json({
      error: `Invalid request: The deposit currency is invalid: ${value.currency}`
    });
    return;
  }
  // TBD: Set a minimum deposit amount?
  if (!DecimalHelper.isPositive(depositAmount)) {  // We do not support "withdrawal"
    res.status(400).json({
      error: `Invalid request: The deposit amount is invalid: ${depositAmount}`
    });
    return;
  }
  if (!DecimalHelper.isValidPrecision(depositAmount)) {
    res.status(400).json({ error: `Invalid depositAmount = ${depositAmount}` });
    return;
  }

  UserHandler.addToWallet(uid, value, (record: UserWallet) => {
    if (record) {
      res.json(record);
    } else {
      res.status(500).json({ error: `Failed to add to wallet for uid = ${uid}` });
    }
  });
});

// Get all open orders of the user
router.route("/:uid/orders").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const uid = req.params.uid;
  console.log(`uid = ${uid}`);

  // tbd:
  OrderHandler.getUserOrders(uid, (records: OrderRecord[]) => {
    if (records) {
      res.json(records);
    } else {
      res.status(404).json({ error: `Orders not found for uid = ${uid}` });
    }
  });
});

// Get all transactions
router.route("/:uid/transactions").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const uid = req.params.uid;
  console.log(`uid = ${uid}`);

  // tbd:
  TransactionHandler.getUserTransactions(uid, (records: TransactionRecord[]) => {
    if (records) {
      res.json(records);
    } else {
      res.status(404).json({ error: `Transactions not found for uid = ${uid}` });
    }
  });
});

export default router;
