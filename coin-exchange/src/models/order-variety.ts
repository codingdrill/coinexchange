import Joi from "joi";
import OrderType from "../common/core/order-type";

class OrderVariety {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    orderType: Joi.number().required(),
    rateLimit: Joi.number(),
  });

  // Factory method.
  public static makeOrderVariety(obj: {}): OrderVariety {
    const cloned = Object.assign({}, obj) as OrderVariety;
    return cloned;
  }

  constructor(public orderType: OrderType, public rateLimit: number = 0) {
  }

}

export default OrderVariety;
