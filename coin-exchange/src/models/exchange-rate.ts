import Joi from "joi";
import CoinCurrency from "../common/core/coin-currency";

class ExchangeRate {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    targetCurrency: Joi.string().alphanum().min(3).max(3).required(),
    baseCurrency: Joi.string().alphanum().min(3).max(3).required(),
    rate: Joi.number().required(),
    lastUpdated: Joi.date(),
  });

  // Factory method.
  public static makeExchangeRate(obj: {}): ExchangeRate {
    const cloned = Object.assign({}, obj) as ExchangeRate;
    return cloned;
  }

  constructor(public targetCurrency: CoinCurrency,
              public baseCurrency: CoinCurrency,
              public rate: number,
              public lastUpdated: Date) {
  }

}

export default ExchangeRate;
