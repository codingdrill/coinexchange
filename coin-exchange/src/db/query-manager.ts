import CoinCurrency from "../common/core/coin-currency";
import OrderSide from "../common/core/order-side";
import OrderStatus from "../common/core/order-status";
import OrderType from "../common/core/order-type";
import DatabaseHelper from "../helpers/database-helper";
import OrderRecord from "../models/order-record";

class QueryManager {

  public static getUserWallet(uid: string, cb: (result: {}) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.all(`SELECT uid, currency, amount FROM user_wallet WHERE uid = '${uid}'`, (err, rows) => {
          if (err) {
            cb(null);
          }
          const list: Array<{}> = [];
          if (rows && rows.length > 0) {
            for (const row of rows) {
              console.log(`Fetched: ${row.uid}, ${row.currency}, ${row.amount}`);
              list.push({ currency: row.currency, amount: row.amount });
            }
          }
          const record = { uid, balance: list };
          console.log(`getUserWallet: uid = ${uid}: record = `, record);
          cb(record);
        });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  // Note: the transaction list associated with an order
  //       can be obtained by joining the transactions table.
  //       (We can even return TransactionRecord[] instead of string[].)
  //       For now, we won't deal with such complexities.

  public static getOrder(orderId: string, cb: (result: {}) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.get(`SELECT
              id,
              uid,
              side,
              order_type AS orderType,
              rate_limit AS rateLimit,
              target_currency AS targetCurrency,
              target_amount AS targetAmount,
              remaining_amount AS remainingAmount,
              exchange_currency AS exchangeCurrency,
              created_at AS createdAt,
              closed_at AS closedAt,
              status
          FROM orders WHERE id = '${orderId}'`, (err, row) => {
            if (err) {
              cb(null);
            }
            if (row) {
              console.log(`Fetched: ${row.id}, ${row.uid}`);

              const record = {
                id: orderId,
                uid: row.uid,
                side: row.side,
                variety: { orderType: row.orderType, rateLimit: row.rateLimit },
                target: { currency: row.targetCurrency, amount: row.targetAmount },
                remainingAmount: row.remainingAmount,
                exchangeCurrency: row.exchangeCurrency,
                createdAt: row.createdAt,
                closedAt: row.closedAt,
                status: row.status,
                transactions: [] as string[]   // temporary
              };
              console.log(`getOrder: id = ${orderId}: record = `, record);
              cb(record);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static getUserOrders(uid: string, cb: (result: any[]) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.all(`SELECT
              id,
              uid,
              side,
              order_type AS orderType,
              rate_limit AS rateLimit,
              target_currency AS targetCurrency,
              target_amount AS targetAmount,
              remaining_amount AS remainingAmount,
              exchange_currency AS exchangeCurrency,
              created_at AS createdAt,
              closed_at AS closedAt,
              status
          FROM orders WHERE uid = '${uid}'`, (err, rows) => {
            if (err) {
              cb(null);
            }
            if (rows) {
              console.log(`Fetched: rows.length = ${rows.length}`);

              const records: any[] = [];
              for (const row of rows) {
                const record = {
                  id: row.id,
                  uid: row.uid,
                  side: row.side,
                  variety: { orderType: row.orderType, rateLimit: row.rateLimit },
                  target: { currency: row.targetCurrency, amount: row.targetAmount },
                  remainingAmount: row.remainingAmount,
                  exchangeCurrency: row.exchangeCurrency,
                  createdAt: row.createdAt,
                  closedAt: row.closedAt,
                  status: row.status,
                  transactions: [] as string[]   // temporary
                };
                records.push(record);
              }
              console.log(`getUserOrders: uid = ${uid}: records = `, records);
              cb(records);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static getOrdersForCurrencies(targetCurrency: CoinCurrency,
                                       exchangeCurrency: CoinCurrency,
                                       status: OrderStatus,
                                       cb: (result: any[]) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.all(`SELECT
              id,
              uid,
              side,
              order_type AS orderType,
              rate_limit AS rateLimit,
              target_currency AS targetCurrency,
              target_amount AS targetAmount,
              remaining_amount AS remainingAmount,
              exchange_currency AS exchangeCurrency,
              created_at AS createdAt,
              closed_at AS closedAt,
              status
          FROM orders WHERE target_currency = '${targetCurrency}'
            AND exchange_currency = '${exchangeCurrency}'
            AND status = ${status}`, (err, rows) => {
            if (err) {
              cb(null);
            }
            if (rows) {
              console.log(`Fetched: rows.length = ${rows.length}`);

              const records: any[] = [];
              for (const row of rows) {
                const record = {
                  id: row.id,
                  uid: row.uid,
                  side: row.side,
                  variety: { orderType: row.orderType, rateLimit: row.rateLimit },
                  target: { currency: row.targetCurrency, amount: row.targetAmount },
                  remainingAmount: row.remainingAmount,
                  exchangeCurrency: row.exchangeCurrency,
                  createdAt: row.createdAt,
                  closedAt: row.closedAt,
                  status: row.status,
                  transactions: [] as string[]   // temporary
                };
                records.push(record);
              }
              console.log(`getOrdersForCurrencies: targetCurrency = ${targetCurrency},
                exchangeCurrency = ${exchangeCurrency}, status = ${status}: records = `, records);
              cb(records);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  // For trading...
  // There are two sides, buy and sell,
  // and each side can be market order or limit order,
  // and hence there are 8 combinations to think about, which is mind boggling :)
  // I divided them into two functions to make it easier to reason about.
  // It may very well be possible to combine these two into one,
  // but it will make it much harder to read and understand, I think.

  // Explanations on my algorithms below: [1] and [2].
  // (These are somewhat arbitrary and may not be what is actually used in real exchanges,
  //    but they seem reasonable to me (based on my understanding of trading),
  //    and I think they are clearly one (reasonably fair) way to determine price/rate.)

  // [1] When the original order is a market order:
  // If there is at least one market order on the opposite side, there will always be a match.
  // Any limit order whose rate falls within the current rate (depending on the side),
  //    will also be matchable.
  // If there is more than one possible matches,
  //    then we will use the oldest such opposite-side order as a matching order.
  // The price/rate will be determined by the last transaction for the same currency pair.
  //    (If none exists, we will start from 1.0.)
  // If there is no order satifying the above criteria, but there are limit orders,
  //    then the lowest/highest limit order (depending on the side) becomes the new market price/rate,
  //    and any limit order for that rate will be a potential match.
  //    If there is more than one, we will use the oldest one as the best match.
  // If there is no orders on the opposite side, then this function will just return null (via callback).
  public static findBestMatchingOrderForMarketOrder(currentRate: number,
                                                    side: OrderSide,
                                                    targetCurrency: CoinCurrency,
                                                    exchangeCurrency: CoinCurrency,
                                                    cb: (result: any, rate: number) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.get(`SELECT
              id,
              uid,
              side,
              order_type AS orderType,
              rate_limit AS rateLimit,
              target_currency AS targetCurrency,
              target_amount AS targetAmount,
              remaining_amount AS remainingAmount,
              exchange_currency AS exchangeCurrency,
              created_at AS createdAt,
              closed_at AS closedAt,
              status
          FROM orders WHERE side = ${side}
            AND target_currency = '${targetCurrency}'
            AND exchange_currency = '${exchangeCurrency}'
            AND status = ${OrderStatus.Open}
            AND (order_type = ${OrderType.Market} OR
              ${(side === OrderSide.Buy) ? " rate_limit >= " + currentRate
            : " rate_limit <= " + currentRate})
            ORDER BY created_at
            LIMIT 1`, (err1, row1) => {
            if (err1) {
              cb(null, 0);
            }
            let record1: OrderRecord = null;
            if (row1) {
              console.log("Fetched - 1");

              record1 = {
                id: row1.id,
                uid: row1.uid,
                side: row1.side,
                variety: { orderType: row1.orderType, rateLimit: row1.rateLimit },
                target: { currency: row1.targetCurrency, amount: row1.targetAmount },
                remainingAmount: row1.remainingAmount,
                exchangeCurrency: row1.exchangeCurrency,
                createdAt: row1.createdAt,
                closedAt: row1.closedAt,
                status: OrderStatus.Open,
                transactions: [] as string[]   // temporary
              };

              console.log(`findBestMatchingOrderForMarketOrder():
                currentRate = ${currentRate}
                side = ${side},
                targetCurrency = ${targetCurrency},
                exchangeCurrency = ${exchangeCurrency}: record = `, record1);
              cb(record1, currentRate);
            } else {
              db.get(`SELECT
                  id,
                  uid,
                  side,
                  order_type AS orderType,
                  rate_limit AS rateLimit,
                  target_currency AS targetCurrency,
                  target_amount AS targetAmount,
                  remaining_amount AS remainingAmount,
                  exchange_currency AS exchangeCurrency,
                  created_at AS createdAt,
                  closed_at AS closedAt,
                  status
                FROM orders WHERE side = ${side}
                  AND target_currency = '${targetCurrency}'
                  AND exchange_currency = '${exchangeCurrency}'
                  AND status = ${OrderStatus.Open}
                  ORDER BY rate_limit ${(side === OrderSide.Buy) ? " DESC" : ""}, created_at
                  LIMIT 1`, (err2, row2) => {
                  if (err2) {
                    cb(null, 0);
                  }
                  let record2: OrderRecord = null;
                  if (row2) {
                    console.log("Fetched - 2");

                    record2 = {
                      id: row2.id,
                      uid: row2.uid,
                      side: row2.side,
                      variety: { orderType: row2.orderType, rateLimit: row2.rateLimit },
                      target: { currency: row2.targetCurrency, amount: row2.targetAmount },
                      remainingAmount: row2.remainingAmount,
                      exchangeCurrency: row2.exchangeCurrency,
                      createdAt: row2.createdAt,
                      closedAt: row2.closedAt,
                      status: OrderStatus.Open,
                      transactions: [] as string[]   // temporary
                    };

                    console.log(`findBestMatchingOrderForMarketOrder():
                      currentRate = ${currentRate}
                      side = ${side},
                      targetCurrency = ${targetCurrency},
                      exchangeCurrency = ${exchangeCurrency}: record = `, record2);
                    cb(record2, record2.variety.rateLimit);
                  } else {
                    cb(null, 0);
                  }
                });
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  // [2] When the original order is a limit order:
  // If the ask/bid price/rate is below/above the current rate,
  //    then the original order will be effectively treated as a market order. Go to [1].
  // If there are market orders, or limit orders that satisfy the limit, on the opposite side,
  //    then they can be potential match.
  //    We will use the oldest such opposite-side order as the best matching order.
  //    The price/rate is the ask/bid price/rate of the original limit order.
  // If there is no matching order on the opposite side, then this function will just return null.
  // (Note: triggerLimitOrderRateLimit is inversed from the original order.rateLimit
  //    because target and exhange have been swapped in these functions.)
  public static findBestMatchingOrderForLimitOrder(currentRate: number,
                                                   side: OrderSide,
                                                   targetCurrency: CoinCurrency,
                                                   exchangeCurrency: CoinCurrency,
                                                   triggerLimitOrderRateLimit: number,
                                                   cb: (result: any, rate: number) => void) {

    if ((side === OrderSide.Buy && triggerLimitOrderRateLimit >= currentRate)
      || (side === OrderSide.Sell && triggerLimitOrderRateLimit <= currentRate)) {
      this.findBestMatchingOrderForMarketOrder(currentRate, side, targetCurrency, exchangeCurrency, cb);
      return;
    }

    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.get(`SELECT
              id,
              uid,
              side,
              order_type AS orderType,
              rate_limit AS rateLimit,
              target_currency AS targetCurrency,
              target_amount AS targetAmount,
              remaining_amount AS remainingAmount,
              exchange_currency AS exchangeCurrency,
              created_at AS createdAt,
              closed_at AS closedAt,
              status
          FROM orders WHERE side = ${side}
            AND target_currency = '${targetCurrency}'
            AND exchange_currency = '${exchangeCurrency}'
            AND status = ${OrderStatus.Open}
            AND (order_type = ${OrderType.Market} OR
              ${(side === OrderSide.Buy) ? " rate_limit >= " + triggerLimitOrderRateLimit
            : " rate_limit <= " + triggerLimitOrderRateLimit})
            ORDER BY created_at
            LIMIT 1`, (err, row) => {
            if (err) {
              cb(null, 0);
            }
            let record: OrderRecord = null;
            if (row) {
              console.log("Fetched");

              record = {
                id: row.id,
                uid: row.uid,
                side: row.side,
                variety: { orderType: row.orderType, rateLimit: row.rateLimit },
                target: { currency: row.targetCurrency, amount: row.targetAmount },
                remainingAmount: row.remainingAmount,
                exchangeCurrency: row.exchangeCurrency,
                createdAt: row.createdAt,
                closedAt: row.closedAt,
                status: OrderStatus.Open,
                transactions: [] as string[]   // temporary
              };

              console.log(`findBestMatchingOrderForLimitOrder():
                currentRate = ${currentRate}
                side = ${side},
                targetCurrency = ${targetCurrency},
                exchangeCurrency = ${exchangeCurrency},
                triggerLimitOrderRateLimit = ${triggerLimitOrderRateLimit}: record = `, record);
              cb(record, triggerLimitOrderRateLimit);
            } else {
              cb(null, 0);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static getTransaction(transId: string, cb: (result: {}) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.get(`SELECT
              id,
              buy_order_id AS buyOrderId,
              sell_order_id AS sellOrderId,
              buyer_id AS buyerId,
              seller_id AS sellerId,
              buy_currency AS buyCurrency,
              buy_amount AS buyAmount,
              sell_currency AS sellCurrency,
              sell_amount AS sellAmount,
              completed_at AS completedAt
          FROM transactions WHERE id = '${transId}'`, (err, row) => {
            if (err) {
              cb(null);
            }
            if (row) {
              console.log(`Fetched: ${row.id}, ${row.uid}`);

              const record = {
                id: transId,
                buyOrderId: row.buyOrderId,
                sellOrderId: row.sellOrderId,
                buyerId: row.buyerId,
                sellerId: row.sellerId,
                buyCurrency: row.buyCurrency,
                buyAmount: row.buyAmount,
                sellCurrency: row.sellCurrency,
                sellAmount: row.sellAmount,
                completedAt: row.completedAt
              };
              console.log(`getTransaction: id = ${transId}: record = `, record);
              cb(record);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static getLatestTransactionForCurrencies(targetCurrency: CoinCurrency,
                                                  baseCurrency: CoinCurrency,
                                                  cb: (result: {}) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.get(`SELECT
              id,
              buy_order_id AS buyOrderId,
              sell_order_id AS sellOrderId,
              buyer_id AS buyerId,
              seller_id AS sellerId,
              buy_currency AS buyCurrency,
              buy_amount AS buyAmount,
              sell_currency AS sellCurrency,
              sell_amount AS sellAmount,
              completed_at AS completedAt
          FROM transactions
            WHERE (buy_currency = '${targetCurrency}' AND sell_currency = '${baseCurrency}') OR
            (buy_currency = '${baseCurrency}' AND sell_currency = '${targetCurrency}')
            ORDER BY completed_at DESC LIMIT 1`, (err, row) => {
            if (err) {
              cb(null);
            }
            if (row) {
              console.log(`Fetched: ${row.id}, ${row.uid}`);

              const record = {
                id: row.id,
                buyOrderId: row.buyOrderId,
                sellOrderId: row.sellOrderId,
                buyerId: row.buyerId,
                sellerId: row.sellerId,
                buyCurrency: row.buyCurrency,
                buyAmount: row.buyAmount,
                sellCurrency: row.sellCurrency,
                sellAmount: row.sellAmount,
                completedAt: row.completedAt
              };
              console.log(`getLatestTransactionForCurrencies:
                targetCurrency = ${targetCurrency},
                baseCurrency = ${baseCurrency}: record = `, record);
              cb(record);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static getUserTransactions(uid: string, cb: (result: any[]) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        db.all(`SELECT
              id,
              buy_order_id AS buyOrderId,
              sell_order_id AS sellOrderId,
              buyer_id AS buyerId,
              seller_id AS sellerId,
              buy_currency AS buyCurrency,
              buy_amount AS buyAmount,
              sell_currency AS sellCurrency,
              sell_amount AS sellAmount,
              completed_at AS completedAt
          FROM transactions WHERE buyer_id = '${uid}' OR seller_id = '${uid}'`, (err, rows) => {
            if (err) {
              cb(null);
            }
            if (rows) {
              console.log(`Fetched: rows.length = ${rows.length}`);

              const records: any[] = [];
              for (const row of rows) {
                const record = {
                  id: row.id,
                  buyOrderId: row.buyOrderId,
                  sellOrderId: row.sellOrderId,
                  buyerId: row.buyerId,
                  sellerId: row.sellerId,
                  buyCurrency: row.buyCurrency,
                  buyAmount: row.buyAmount,
                  sellCurrency: row.sellCurrency,
                  sellAmount: row.sellAmount,
                  completedAt: row.completedAt
                };
                records.push(record);
              }
              console.log(`getUserTransactions: uid = ${uid}: records = `, records);
              cb(records);
            } else {
              cb(null);
            }
          });
      });
    } catch (ex) {
      console.log("Failed to run query.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  private constructor() { }

}

export default QueryManager;
