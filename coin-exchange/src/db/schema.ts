// Schema

// This covers all our use cases,
// but probably it's an overkill for demo purposes.
// We will use a simplified version below.
const createSql = [
  `
  CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    name TEXT
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS user_wallet (
    uid VARCHAR(64) NOT NULL,
    currency VARCHAR(6),
    amount DECIMAL(16,2),
    PRIMARY KEY (uid, currency),
    FOREIGN KEY (uid) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS orders (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    uid VARCHAR(64) NOT NULL,
    side SMALLINT NOT NULL,
    type SMALLINT NOT NULL,
    rate_limit FLOAT,
    target_currency VARCHAR(6),
    target_amount DECIMAL(16,2),
    remaining_amount DECIMAL(16,2),
    exchange_currency VARCHAR(6),
    created_at DATETIME,
    closed_at DATETIME,
    status SMALLINT,
    FOREIGN KEY (uid) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE INDEX idx_orders_uid_status on orders (uid, status);
  `,
  `
  CREATE INDEX idx_orders_currencies on orders (target_currency, exchange_currency, status, created_at DESC);
  `,
  `
  CREATE TABLE IF NOT EXISTS transactions (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    buy_order_id VARCHAR(64) NOT NULL,
    sell_order_id VARCHAR(64) NOT NULL,
    buyer_id VARCHAR(64) NOT NULL,
    seller_id VARCHAR(64) NOT NULL,
    buy_currency VARCHAR(6),
    buy_amount DECIMAL(16,2),
    sell_currency VARCHAR(6),
    sell_amount DECIMAL(16,2),
    completed_at DATETIME,
    FOREIGN KEY (buy_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (sell_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (buyer_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (seller_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE INDEX idx_transactions_buy_order on transactions (buy_order_id);
  `,
  `
  CREATE INDEX idx_transactions_sell_order on transactions (sell_order_id);
  `,
  `
  CREATE INDEX idx_transactions_buyer on transactions (buyer_id);
  `,
  `
  CREATE INDEX idx_transactions_seller on transactions (seller_id);
  `,
  `
  CREATE INDEX idx_transactions_currencies on transactions (buy_currency, sell_currency, completed_at DESC);
  `,
  `
  CREATE TABLE IF NOT EXISTS treasury (
    transaction_id VARCHAR(64) NOT NULL,
    buy_currency VARCHAR(6),
    buy_delta DECIMAL(16,2),
    sell_currency VARCHAR(6),
    sell_delta DECIMAL(16,2),
    completed_at DATETIME,
    FOREIGN KEY (transaction_id) REFERENCES transactions (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
];

// We really don't need indexes for small data example.
const createWithoutIndexesSql = [
  `
  CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    name TEXT
  );
  `
  ,
  `
  CREATE TABLE IF NOT EXISTS user_wallet (
    uid VARCHAR(64) NOT NULL,
    currency VARCHAR(6),
    amount DECIMAL(16,2),
    PRIMARY KEY (uid, currency),
    FOREIGN KEY (uid) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS orders (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    uid VARCHAR(64) NOT NULL,
    side SMALLINT NOT NULL,
    type SMALLINT NOT NULL,
    rate_limit FLOAT,
    target_currency VARCHAR(6),
    target_amount DECIMAL(16,2),
    remaining_amount DECIMAL(16,2),
    exchange_currency VARCHAR(6),
    created_at DATETIME,
    closed_at DATETIME,
    status SMALLINT,
    FOREIGN KEY (uid) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS transactions (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    buy_order_id VARCHAR(64) NOT NULL,
    sell_order_id VARCHAR(64) NOT NULL,
    buyer_id VARCHAR(64) NOT NULL,
    seller_id VARCHAR(64) NOT NULL,
    buy_currency VARCHAR(6),
    buy_amount DECIMAL(16,2),
    sell_currency VARCHAR(6),
    sell_amount DECIMAL(16,2),
    completed_at DATETIME,
    FOREIGN KEY (buy_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (sell_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (buyer_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (seller_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS treasury (
    transaction_id VARCHAR(64) NOT NULL,
    buy_currency VARCHAR(6),
    buy_delta DECIMAL(16,2),
    sell_currency VARCHAR(6),
    sell_delta DECIMAL(16,2),
    completed_at DATETIME,
    FOREIGN KEY (transaction_id) REFERENCES transactions (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
];

// For demo purposes,
// We don't need full sql machinery.
// We don't need indexes, and we don't need user foreign key constraints.
// This should be fine.
// (Still kept foreign key constraints on orders and transactions.)
const createWithoutUserForeignKeysSql = [
  `
  CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    name TEXT
  );
  `
  ,
  `
  CREATE TABLE IF NOT EXISTS user_wallet (
    uid VARCHAR(64) NOT NULL,
    currency VARCHAR(6),
    amount DECIMAL(16,2),
    PRIMARY KEY (uid, currency)
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS orders (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    uid VARCHAR(64) NOT NULL,
    side SMALLINT NOT NULL,
    order_type SMALLINT NOT NULL,
    rate_limit FLOAT,
    target_currency VARCHAR(6),
    target_amount DECIMAL(16,2),
    remaining_amount DECIMAL(16,2),
    exchange_currency VARCHAR(6),
    created_at DATETIME,
    closed_at DATETIME,
    status SMALLINT
  );
  `,
  `
  CREATE TABLE IF NOT EXISTS transactions (
    id VARCHAR(64) NOT NULL PRIMARY KEY,
    buy_order_id VARCHAR(64) NOT NULL,
    sell_order_id VARCHAR(64) NOT NULL,
    buyer_id VARCHAR(64) NOT NULL,
    seller_id VARCHAR(64) NOT NULL,
    buy_currency VARCHAR(6),
    buy_amount DECIMAL(16,2),
    sell_currency VARCHAR(6),
    sell_amount DECIMAL(16,2),
    completed_at DATETIME,
    FOREIGN KEY (buy_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (sell_order_id) REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE
  );
  `,
  // `
  // CREATE TABLE IF NOT EXISTS treasury (
  //   transaction_id VARCHAR(64) NOT NULL,
  //   buy_currency VARCHAR(6),
  //   buy_delta DECIMAL(16,2),
  //   sell_currency VARCHAR(6),
  //   sell_delta DECIMAL(16,2),
  //   completed_at DATETIME,
  //   FOREIGN KEY (transaction_id) REFERENCES transactions (id) ON DELETE CASCADE ON UPDATE CASCADE
  // );
  // `,
];

const getSqlForCreateTables = () => {
  return createSql;
};

const getSqlForCreateWithoutIndexes = () => {
  return createWithoutIndexesSql;
};

const getSqlForCreateWithoutUserForeignKeys = () => {
  return createWithoutUserForeignKeysSql;
};

export default {
  getSqlForCreateTables,
  getSqlForCreateWithoutIndexes,
  getSqlForCreateWithoutUserForeignKeys
};
